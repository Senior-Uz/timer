"use strict";

//  SELECTORS START
let hour = document.querySelector(".hour");
let minute = document.querySelector(".minute");
let second = document.querySelector(".second");

let add = document.querySelector(".button-add");
let startStop = document.querySelector(".button-startStop");
let reset = document.querySelector(".button-reset");
//  SELECTORS END

let timer;

// changing start stop button
add.addEventListener("click", () =>
  alert("this button has not any functionality yet!")
);

startStop.addEventListener("click", function () {
  if (startStop.innerHTML == "Start") {
    if (hour.value == "00" && minute.value == "00" && second.value == "00") {
      alert("please set a timer");
    } else {
      startStop.innerHTML = "Pause";
      startInterval();
    }
  } else if (startStop.innerHTML == "Pause") {
    startStop.innerHTML = "Start";
    stopInterval();
  }
});

// =======================================

function start() {
  if (hour.value == 0 && minute.value == 0 && second.value == 0) {
    hour.value = "00";
    minute.value = "00";
    second.value = "00";

    if (startStop.innerHTML == "Start" || startStop.innerHTML == "Pause") {
      startStop.innerHTML = "Start";
      stopInterval();
    }
  } else if (second.value != 0) {
    second.value--;
  } else if (minute.value != 0 && second.value == 0) {
    second.value = 59;
    minute.value--;
  } else if (hour.value != 0 && minute.value == 0) {
    minute.value = 60;
    hour.value--;
  }
  return;
}

// ==== RESETTING TIMER

reset.addEventListener("click", function () {
  hour.value = "00";
  minute.value = "00";
  second.value = "00";
  stopInterval();
  if (startStop.innerHTML == "Start" || startStop.innerHTML == "Pause") {
    startStop.innerHTML = "Start";
  }
});

// ==== setting interval===

function startInterval() {
  timer = setInterval(() => {
    start();
  }, 1000);
}

// === STOPPING interval function
function stopInterval() {
  clearInterval(timer);
}
